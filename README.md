# duraflex AX database off-line history data batch processing pipeline design

## Data warehouse architecture
1. **ODS Layer**: Operation Data Store layer, 採集原始數據來源的數據，做基本處理後儲存。
2. **DWS Layer**: Data Warehouse Service layer, 進一步處理ODS layer的數據並生成一些共用表，增加復用性，並構建出一些寬表用於高階數據分析。 
3. **APP Layer**: Application layer, 產生API接口用來直接對外提供查詢，eg., 提供UI串接，Buiness intelligence(Tableau), 數據監控。

![示意圖](./DW.png)
